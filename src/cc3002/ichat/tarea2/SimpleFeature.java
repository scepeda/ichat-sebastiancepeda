package cc3002.ichat.tarea2;

import cc3002.ichat.tarea3.Message;

/**
 * Class that represents a concrete Feature that doesn't do anything with the
 * input. Default filter.
 * 
 * @author Sebastian Cepeda F.
 * 
 */
public class SimpleFeature implements IFeature {
	/**
	 * Return the input String without processing.
	 */
	@Override
	public Message Apply(Message input) {
		return input;
	}

}

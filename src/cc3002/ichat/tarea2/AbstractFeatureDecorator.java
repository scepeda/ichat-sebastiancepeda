package cc3002.ichat.tarea2;

import cc3002.ichat.tarea3.Message;

/**
 * Declaration of the AbstractFeature Decorator
 * 
 * @author Sebastian Cepeda F.
 * 
 */
public abstract class AbstractFeatureDecorator implements IFeature {
	protected IFeature feature;

	protected AbstractFeatureDecorator(IFeature feat) {
		this.feature = feat;
	}

	@Override
	public Message Apply(Message input) {
		return feature.Apply(input);
	}
}

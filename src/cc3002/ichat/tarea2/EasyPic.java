package cc3002.ichat.tarea2;

import cc3002.ichat.tarea3.Message;
	/**
	 * Class that generate html code for an image display.
	 * @author Sebastian Cepeda F.
	 *
	 */
public class EasyPic extends AbstractFeatureDecorator {

	public EasyPic(IFeature feat) {
		super(feat);
	}

	@Override
	public Message Apply(Message input) {
		input = super.Apply(input);
		String result = input.message();
		int start = result.indexOf("$");
		while (start != -1) {
			int end = result.indexOf("$", start + 1);
			String imName = result.substring(start, end + 1);
			result = result.replace(imName, this.replace(imName));
			start = result.indexOf("$", end);
		}
		return new Message(input.username(),result);
	}

	/**
	 * Replaces the String with the image name, with html code to display that
	 * image.
	 * 
	 * @param word
	 *            String with the image name.
	 * @return String to
	 */
	private String replace(String word) {
		String result = "<img src=\"file:images/" + word.replace("$", "")
				+ "\" width=50 height=50/>";
		return result;
	}
}

package cc3002.ichat.tarea2;

import java.util.ArrayList;

import cc3002.ichat.tarea3.Message;

/**
 * Class that filter bad words given in a txt file.
 * 
 * @author Sebastian Cepeda F.
 * 
 */
public class BadWordFilter extends AbstractFeatureDecorator {

	/**
	 * Array of bad words.
	 */
	private ArrayList<String> bwfWords;

	public BadWordFilter(IFeature feat, String bwfFile) {
		super(feat);
		bwfWords = Utils.WordsFileToList(bwfFile);
	}

	/**
	 * Method that filter tags and checks the list of words.
	 */
	@Override
	public Message Apply(Message input) {
		input = super.Apply(input);
		String text = input.message();
		int start = text.indexOf("<");
		while (start != -1) {
			int end = text.indexOf(">", start + 1);
			String tag = text.substring(start, end + 1);
			text = text.replace(tag, "");
			start = text.indexOf("<", end);
		}
		String result = input.message();
		String[] words = text.replace(",", " ").split(" ");
		for (int iWord = 0; iWord < words.length; iWord++) {
			boolean wordInDictionary = false;
			for (int iDictionary = 0; iDictionary < bwfWords.size(); iDictionary++) {
				if (words[iWord].replace(" ", "").replace(".", "")
						.toLowerCase()
						.compareTo(bwfWords.get(iDictionary).toLowerCase()) == 0) {
					wordInDictionary = true;
				}
			}
			if (wordInDictionary) {
				result = result.replace(words[iWord],
						this.replace(words[iWord]));
			}
		}
		return new Message(input.username(), result);
	}

	/**
	 * Replaces the word with N '#' characters, where N is the String length.
	 * 
	 * @param word
	 *            String to be replaced.
	 * @return String with #'s.
	 */
	private String replace(String word) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < word.length(); i++) {
			result.append("#");
		}
		return result.toString();
	}
}

package cc3002.ichat.tarea2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Utils class, methods used for other classes.
 * 
 * @author Sebastian Cepeda F.
 * 
 */
public class Utils {
	/**
	 * Fills an ArrayList with the content of a file, words are separated with
	 * newline.
	 * 
	 * @param FilePath
	 *            Path to the file.
	 * @return ArrayList with the content of the file.
	 */
	static public ArrayList<String> WordsFileToList(String FilePath) {
		ArrayList<String> result = new ArrayList<String>();
		BufferedReader fileReader = null;
		try {
			File file = new File(FilePath);
			fileReader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e1) {
			System.out.println("File not found. " + FilePath);
			return null;
		}
		try {
			String line = fileReader.readLine();
			while (line != null) {
				result.add(line);
				line = fileReader.readLine();
			}
		} catch (IOException e) {
		} finally {
			try {
				fileReader.close();
			} catch (IOException e) {
			}
		}
		return result;
	}
}

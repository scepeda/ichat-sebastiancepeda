package cc3002.ichat.tarea2;

import java.util.ArrayList;

import cc3002.ichat.tarea3.Message;

/**
 * Class that check spelling words, given a dictionary in a txt file.
 * 
 * @author Sebastian Cepeda F.
 * 
 */
public class MiniSpellChecker extends AbstractFeatureDecorator {
	private ArrayList<String> mscWords;

	public MiniSpellChecker(IFeature feat, String mscFile) {
		super(feat);
		mscWords = Utils.WordsFileToList(mscFile);
	}

	/**
	 * Method that filter tags and checks the list of words.
	 */
	@Override
	public Message Apply(Message input) {
		input = super.Apply(input);
		String text = input.message();
		int start = text.indexOf("<");
		while (start != -1) {
			int end = text.indexOf(">", start + 1);
			String tag = text.substring(start, end + 1);
			text = text.replace(tag, "");
			start = text.indexOf("<", end);
		}
		String result = input.message();
		String[] words = text.replace(",", " ").split(" ");
		for (int iWord = 0; iWord < words.length; iWord++) {
			boolean wordInDictionary = false;
			for (int iDictionary = 0; iDictionary < mscWords.size(); iDictionary++) {
				if (words[iWord].replace(" ", "").replace(".", "")
						.toLowerCase()
						.compareTo(mscWords.get(iDictionary).toLowerCase()) == 0) {
					wordInDictionary = true;
				}
			}
			if (!wordInDictionary) {
				result = result.replace(words[iWord],
						this.replace(words[iWord]));
			}
		}
		return new Message(input.username(), result);
	}

	private String replace(String word) {
		word = word.replace(" ", "");
		if (word.compareTo("") == 0) {
			return word;
		}
		if (word.toLowerCase().contains("#")) {
			return word;
		}
		return "<font color=\"red\">" + word + "</font>";
	}
}

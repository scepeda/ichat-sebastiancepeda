package cc3002.ichat.tarea2;

import cc3002.ichat.tarea3.Message;

public interface IFeature {
	Message Apply(Message input);
}

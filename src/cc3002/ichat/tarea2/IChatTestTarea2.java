package cc3002.ichat.tarea2;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import cc3002.ichat.core.ChatClient;
import cc3002.ichat.core.ChatServer;
import cc3002.ichat.core.UIChatGroup;
import cc3002.ichat.tarea3.Message;

public class IChatTestTarea2 {
	ChatServer server;
	Thread t1;
	String[] args = { "localhost", "2051", "sebastian", "-all" };

	@Before
	public void init() {
		server = new ChatServer(Integer.parseInt(args[1]));
		t1 = new Thread(server);
	}

	@Test
	public void testServer() {
		assertNotNull(server);
	}

	@Test
	public void testBadWordFilter() throws UnknownHostException, IOException {
		String bwfFile = "config/BadWords.txt";
		IFeature features = new SimpleFeature();
		features = new BadWordFilter(features, bwfFile);
		assertTrue(features
				.Apply(new Message("", "Garabatos: ctm,CTM,Ctm, wea"))
				.message().equals("Garabatos: ###,###,###, ###"));
		assertTrue(features
				.Apply(new Message("", "Sin Garabatos: hola se�or que tal"))
				.message().equals("Sin Garabatos: hola se�or que tal"));
	}

	@Test
	public void testMiniSpellChecker() {
		String mscFile = "config/MSCDictionary.txt";
		IFeature features = new SimpleFeature();
		features = new MiniSpellChecker(features, mscFile);
		assertTrue(features
				.Apply(new Message("", "Aqu�,sovre,sobre,### "))
				.message()
				.equals("Aqu�,<font color=\"red\">" + "sovre"
						+ "</font>,sobre,### "));
	}

	@Test
	public void testEasyPic() {
		IFeature features = new SimpleFeature();
		features = new EasyPic(features);
		assertTrue(features
				.Apply(new Message("", "Auxiliar: $auxiliar.png$"))
				.message()
				.equals("Auxiliar: <img src=\"file:images/auxiliar.png\" width=50 height=50/>"));
	}

	@Test
	public void testUtils() {
		ArrayList<String> list;
		list = Utils.WordsFileToList("FileThatShouldntExist.txt");
		assertNull(list);
		String mscFile = "config/MSCDictionary.txt";
		list = Utils.WordsFileToList(mscFile);
		assertNotNull(list);
	}

	@Test
	public void testClient() throws UnknownHostException, IOException {
		t1.start();
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String username = args[2];
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("bwfFile", "config/BadWords.txt");
		parameters.put("mscFile", "config/MSCDictionary.txt");
		parameters.put("logFileName", "logs/ichat.log");
		Socket socket = new Socket(host, port);
		ChatClient client = new ChatClient(username, socket.getInputStream(),
				socket.getOutputStream());
		UIChatGroup ui = new UIChatGroup(client);
		ui.loadFeatures(args, parameters);
		// ui.setVisible(true);
		client.sendMessage("Aqu� va el auxiliar... $auxiliar.png$");
		client.sendMessage("Aqu� va el ayudante... $ayudante.png$");
		client.sendMessage("Aqu� va el alumno... $alumno.png$");
		client.sendMessage("ctm");
		client.sendMessage("Wn ctm, chucha wn, la wea...");
		client.sendMessage("tambien deber�a tener acento");
		client.sendMessage("sovre es con b");
		client.sendMessage("sovre");
		client.sendMessage("sobre");
		client.sendMessage("code auxiliar: $auxiliar.png$");
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
		}
	}
}

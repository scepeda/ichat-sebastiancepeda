package cc3002.ichat.tarea3;

import cc3002.ichat.tarea2.AbstractFeatureDecorator;
import cc3002.ichat.tarea2.IFeature;

/**
 * Class that filter a sent message for the keyword String "code ".
 * 
 * @author Sebastian Cepeda F.
 * 
 */
public class CodeFeatureSend extends AbstractFeatureDecorator {

	public CodeFeatureSend(IFeature feat) {
		super(feat);
	}

	@Override
	public Message Apply(Message input) {
		input = super.Apply(input);
		String result = input.message();
		if (result.trim().toLowerCase().indexOf("code ") == 0) {
			int index = result.toLowerCase().indexOf("code ");
			StringBuilder resultBuilder = new StringBuilder();
			resultBuilder.append("<code>");
			resultBuilder.append(result.substring(index + 5));
			resultBuilder.append("</code>");
			return super.Apply(new Message(input.username(), resultBuilder
					.toString()));
		}
		return new Message(input.username(), result);
	}
}

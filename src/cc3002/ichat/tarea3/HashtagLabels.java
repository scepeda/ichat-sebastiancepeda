package cc3002.ichat.tarea3;

import java.util.ArrayList;

import cc3002.ichat.tarea2.AbstractFeatureDecorator;
import cc3002.ichat.tarea2.IFeature;

/**
 * Class that filter Hashtag and Label messages.
 * 
 * @author Sebastian Cepeda F.
 * 
 */
public class HashtagLabels extends AbstractFeatureDecorator {

	public HashtagLabels(IFeature feat) {
		super(feat);
	}

	@Override
	public Message Apply(Message input) {
		ArrayList<String> endings = new ArrayList<String>();
		endings.add(" ");
		endings.add(".");
		endings.add(",");
		endings.add("\n");
		input = super.Apply(input);
		String result = input.message();
		ArrayList<String> wordList = new ArrayList<String>();
		int start = result.indexOf("#");
		while (start != -1) {
			int end = -1;
			for (String e : endings) {
				end = result.indexOf(e, start + 1);
				if (end > 0) {
					break;
				}
			}
			if (end > 0) {
				String imName = result.substring(start, end + 1);
				if (!wordList.contains(imName)) {
					wordList.add(imName);
				}
			} else {
				break;
			}
			start = result.indexOf("#", end);
		}
		start = result.indexOf("@");
		while (start != -1) {
			int end = -1;
			for (String e : endings) {
				end = result.indexOf(e, start + 1);
				if (end > 0) {
					break;
				}
			}
			if (end > 0) {
				String imName = result.substring(start, end + 1);
				if (!wordList.contains(imName)) {
					wordList.add(imName);
				}
			} else {
				break;
			}
			start = result.indexOf("@", end);
		}
		for (String e : wordList) {
			result = result.replace(e, this.replace(e));
		}
		return new Message(input.username(), result);
	}

	private String replace(String word) {
		String result = "<em>" + word + "</em>";
		return result;
	}
}

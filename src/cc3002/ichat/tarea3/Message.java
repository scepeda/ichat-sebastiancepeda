package cc3002.ichat.tarea3;

/**
 * Class that represents a message from a user.
 * 
 * @author Sebastian Cepeda F.
 * 
 */
public class Message {
	public Message(String username, String message) {
		this.username = username;
		this.message = message;
	}

	public Message(Message input) {
		this.username = input.username;
		this.message = input.message;
	}

	private String username;
	private String message;

	public String username() {
		return username;
	}

	public String message() {
		return message;
	}
}

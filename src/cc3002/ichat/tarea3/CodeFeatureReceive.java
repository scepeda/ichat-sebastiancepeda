package cc3002.ichat.tarea3;

import cc3002.ichat.tarea2.AbstractFeatureDecorator;
import cc3002.ichat.tarea2.IFeature;

/**
 * Class that filter a received message for the tag
 * <code>, avoiding the use of the rest of filters.
 * 
 * @author Sebastian Cepeda F.
 * 
 */
public class CodeFeatureReceive extends AbstractFeatureDecorator {

	public CodeFeatureReceive(IFeature feat) {
		super(feat);
	}

	@Override
	public Message Apply(Message input) {
		if (input.message().contains("<code>")
				&& input.message().contains("</code>")) {
			return input;
		}
		return feature.Apply(input);
	}
}

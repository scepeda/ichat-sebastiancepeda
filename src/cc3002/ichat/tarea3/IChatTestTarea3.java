package cc3002.ichat.tarea3;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;

import org.junit.Test;

import cc3002.ichat.core.ChatClient;
import cc3002.ichat.core.UIChatGroup;
import cc3002.ichat.tarea2.EasyPic;
import cc3002.ichat.tarea2.IChatTestTarea2;
import cc3002.ichat.tarea2.IFeature;
import cc3002.ichat.tarea2.SimpleFeature;

public class IChatTestTarea3 extends IChatTestTarea2 {
	StringBuilder outputLog;

	@Test
	public void testHashtagLabels() {
		IFeature features = new SimpleFeature();
		features = new HashtagLabels(features);
		assertTrue(features
				.Apply(new Message(
						"",
						"Yayita, your #POO instincts are growing. Do you have any questions for @juampi today?"))
				.message()
				.equals("Yayita, your <em>#POO </em>instincts are growing. Do you have any questions for <em>@juampi </em>today?"));
		assertTrue(features
				.Apply(new Message("", "Yayita, your #POO #POO #lol @juampi ?"))
				.message()
				.equals("Yayita, your <em>#POO </em><em>#POO </em><em>#lol </em><em>@juampi </em>?"));
	}

	@Test
	public void testCodeSend() {
		IFeature featuresSend = new SimpleFeature();
		featuresSend = new CodeFeatureSend(featuresSend);
		assertTrue(featuresSend
				.Apply(new Message("", "code tamagotchi.doSport();")).message()
				.equals("<code>tamagotchi.doSport();</code>"));
		assertTrue(featuresSend
				.Apply(new Message("", " code tamagotchi.doSport();"))
				.message().equals("<code>tamagotchi.doSport();</code>"));
		assertTrue(featuresSend.Apply(new Message("", "tamagotchi.doSport();"))
				.message().equals("tamagotchi.doSport();"));

	}

	@Test
	public void testCodeReceive() {
		IFeature features = new SimpleFeature();
		features = new EasyPic(features);
		features = new CodeFeatureReceive(features);
		assertTrue(features
				.Apply(new Message("", "<code>tamagotchi.doSport();</code>"))
				.message().equals("<code>tamagotchi.doSport();</code>"));
		assertTrue(features
				.Apply(new Message("", "<code>Auxiliar: $auxiliar.png$</code>"))
				.message().equals("<code>Auxiliar: $auxiliar.png$</code>"));
	}

	@Test
	public void testLogFeaturePrintStream() {
		outputLog = new StringBuilder();
		IFeature features = new SimpleFeature();
		class NullOutputStream extends OutputStream {
			@Override
			public void write(int b) throws IOException {
				outputLog.append((char) b);
			}
		}
		;
		class NullPrintStream extends PrintStream {
			public NullPrintStream() {
				super(new NullOutputStream());
			}
		}
		;
		PrintStream out = null;
		out = new NullPrintStream();
		features = new LogFeature(features, out);
		Message result = features.Apply(new Message("Username", "Testing log"));
		assertTrue(result.username().equals("Username"));
		assertTrue(result.message().equals("Testing log"));
		String[] outputLogArray = outputLog.toString().split(" - ");
		assertTrue(outputLogArray[0].equals("Username"));
		assertTrue(outputLogArray[1].equals("Testing log"));
	}

	@Test
	public void testLogFeatureFileName() {
		String logFileName = "logs/ichat.log";// If it doesn't exist, it creates
												// it.
		IFeature features = new SimpleFeature();
		try {
			features = new LogFeature(features, logFileName);
		} catch (FileNotFoundException e) {
		}
		long initialSize = new File(logFileName).length();
		Message result = features.Apply(new Message("Username", "Testing log"));
		assertTrue(result.username().equals("Username"));
		assertTrue(result.message().equals("Testing log"));
		assertTrue(new File(logFileName).length() > initialSize);
	}
}

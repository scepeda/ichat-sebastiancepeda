package cc3002.ichat.tarea3;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Date;

import cc3002.ichat.tarea2.AbstractFeatureDecorator;
import cc3002.ichat.tarea2.IFeature;

/**
 * Class that log the messages received by the user.
 * 
 * @author Sebastian Cepeda F.
 * 
 */
public class LogFeature extends AbstractFeatureDecorator {

	private PrintStream outPrintStream;

	public LogFeature(IFeature feat, String logFile)
			throws FileNotFoundException {
		this(feat, new PrintStream(new BufferedOutputStream(
				new FileOutputStream(new File(logFile), true))));
	}

	public LogFeature(IFeature feat, PrintStream out) {
		super(feat);
		outPrintStream = out;
	}

	@Override
	public Message Apply(Message input) {
		input = super.Apply(input);
		StringBuilder a = new StringBuilder();
		a.append(input.username());
		a.append(" - ");
		a.append(input.message());
		a.append(" - ");
		a.append(new Date().toString());
		outPrintStream.println(a);
		outPrintStream.flush();
		return input;
	}
}

package cc3002.ichat.core;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer implements Runnable {
	/**
	 * Int value for the port.
	 */
	private int port;
	/**
	 * Boolean value that represents the execution state.
	 * */
	private boolean Running;

	/**
	 * Constructor of the class.
	 * 
	 * @param port
	 *            Port of the server.
	 */
	public ChatServer(int port) {
		this.port = port;
		this.Running = true;
	}

	/**
	 * Run method, it's called to start the server. Accepts connections and
	 * starts a handler for each one.
	 */
	@Override
	public void run() {
		ServerSocket server = null;
		try {
			server = new ServerSocket(port);
			while (Running) {
				Socket client = server.accept();
				System.out.println("Accepted from " + client.getInetAddress());
				ChatHandler c = new ChatHandler(client);
				c.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				server.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Method to stop the execution of the server.
	 */
	public void stopRun() {
		this.Running = false;
	}

	/**
	 * Main method of the server. Start the server with params given by console.
	 * 
	 * @param args
	 *            Params given: port.
	 * @throws IOException
	 */
	public static void main(String args[]) throws IOException {
		int port = Integer.parseInt(args[0]);
		new ChatServer(port).run();
	}
}

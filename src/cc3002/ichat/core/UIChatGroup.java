package cc3002.ichat.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextArea;

import cc3002.ichat.tarea2.BadWordFilter;
import cc3002.ichat.tarea2.EasyPic;
import cc3002.ichat.tarea2.IFeature;
import cc3002.ichat.tarea2.MiniSpellChecker;
import cc3002.ichat.tarea2.SimpleFeature;
import cc3002.ichat.tarea3.CodeFeatureReceive;
import cc3002.ichat.tarea3.CodeFeatureSend;
import cc3002.ichat.tarea3.HashtagLabels;
import cc3002.ichat.tarea3.LogFeature;
import cc3002.ichat.tarea3.Message;

public class UIChatGroup extends UIFrame implements Observer {
	private ChatClient client;
	public IFeature features, featuresSend;
	private String message;

	public UIChatGroup(ChatClient client) {
		super(client.username());
		client.addObserver(this);
		this.client = client;
		features = new SimpleFeature();
		featuresSend = new SimpleFeature();
	}

	/**
	 * Este metodo se llama cada ves que alguien manda un mensaje al server
	 * 
	 * @param o
	 *            es un objeto ChatClient que recibio el mensaje del server
	 * @param arg
	 *            es el mensaje en si
	 * 
	 *            arg es de tipo String con el siguiente formato username -
	 *            message
	 */
	@Override
	public void update(Observable o, Object arg) {
		message = (String) arg;
		int index = message.indexOf("-");
		String username = message.substring(0, index + 1);
		message = message.substring(index + 1, message.length());
		Message input = new Message(username, message);
		// agregando el mensaje a la ventana
		addHtmlMessage(username, features.Apply(input).message());
		// actualizando la ventana
		updateUI();
	}

	/**
	 * Este metodo se invoca cuando alguien presiona el boton enviar
	 * 
	 * @param text
	 *            es el objeto que tiene dentro el texto escrito por el usuario
	 */
	@Override
	public void send(JTextArea text) {
		Message input = new Message("", text.getText().replace("\n", ""));
		client.sendMessage(featuresSend.Apply(input).message());
		super.send(text);
	}

	/**
	 * Este metodo se llama cuando alguien cierra la ventana
	 */
	@Override
	public void close() {
		// cerrando la sesion con el server
		client.closeSession();
		super.close();
	}

	public static void main(String[] args) throws UnknownHostException,
			IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String username = args[2];
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("bwfFile", "config/BadWords.txt");
		parameters.put("mscFile", "config/MSCDictionary.txt");
		parameters.put("logFileName", "logs/ichat.log");
		Socket socket = new Socket(host, port);
		ChatClient client = new ChatClient(username, socket.getInputStream(),
				socket.getOutputStream());
		UIChatGroup ui = new UIChatGroup(client);
		ui.loadFeatures(args, parameters);
		ui.setVisible(true);
	}

	public void loadFeatures(String[] args, HashMap<String, String> parameters) {
		ArrayList<String> plugins = new ArrayList<String>();
		if (args.length > 3) {
			for (int i = 3; i < args.length; i++) {
				if ((args[i].compareTo("-all") == 0)
						|| (args[i].compareTo("--all") == 0)) {
					plugins.add("--hl");
					plugins.add("--ep");
					plugins.add("--bwf");
					plugins.add("--msc");
					plugins.add("--log");
					plugins.add("--src");
					break;
				}
				if ((args[i].compareTo("-default") == 0)
						|| (args[i].compareTo("--default") == 0)) {
					plugins.add("--hl");
					plugins.add("--ep");
					plugins.add("--bwf");
					plugins.add("--log");
					break;
				}
				if (args[i].compareTo("--bwf") == 0) {
					plugins.add("--bwf");
				}
				if (args[i].compareTo("--msc") == 0) {
					plugins.add("--msc");
				}
				if (args[i].compareTo("--ep") == 0) {
					plugins.add("--ep");
				}
				if (args[i].compareTo("--log") == 0) {
					plugins.add("--log");
				}
				if (args[i].compareTo("--src") == 0) {
					plugins.add("--src");
				}
			}
			for (String plugin : plugins) {
				if (plugin.compareTo("--src") == 0) {
					featuresSend = new CodeFeatureSend(featuresSend);
				}
				if (plugin.compareTo("--ep") == 0) {
					features = new EasyPic(features);
				}
				if (plugin.compareTo("--bwf") == 0) {
					features = new BadWordFilter(features,
							parameters.get("bwfFile"));
				}
				if (plugin.compareTo("--msc") == 0) {
					features = new MiniSpellChecker(features,
							parameters.get("mscFile"));
				}
				if (plugin.compareTo("--hl") == 0) {
					features = new HashtagLabels(features);
				}
				if (plugin.compareTo("--log") == 0) {
					try {
						features = new LogFeature(features,
								parameters.get("logFileName"));
					} catch (FileNotFoundException e) {
						System.out.println(e.getMessage());
					}
				}
			}
		}
		features = new CodeFeatureReceive(features);
	}
}

package cc3002.ichat.core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Observable;

public class ChatClient extends Observable implements Runnable {

	private String user;
	private DataInputStream input;
	private DataOutputStream output;
	private Thread listener;
	/**
	 * Boolean value that represents the execution state.
	 * */
	volatile private boolean Running;

	public ChatClient(String user, InputStream i, OutputStream o) {
		this.user = user;
		this.input = new DataInputStream(new BufferedInputStream(i));
		this.output = new DataOutputStream(new BufferedOutputStream(o));
		listener = new Thread(this);
		listener.start();
		this.Running = true;
	}

	public String username() {
		return user;
	}

	public void run() {
		try {
			sendMessage(user);
			while (Running) {
				setChanged();
				notifyObservers(input.readUTF());
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			listener = null;
			try {
				output.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public void sendMessage(String message) {
		try {
			output.writeUTF(message);
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void closeSession() {
		if (listener != null)
			this.Running = false;
	}

	// public static void main(String args[]) throws IOException {
	// String host = "localhost";
	// String userName = "juampi";
	// int port = 2099;
	// if (args.length > 2) {
	// host = args[0];
	// port = Integer.parseInt(args[1]);
	// userName = args[2];
	// }
	//
	// Socket socket = new Socket(host, port);
	// new ChatClient(userName, socket.getInputStream(),
	// socket.getOutputStream());
	// }
}

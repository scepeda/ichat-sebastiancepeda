package cc3002.ichat.core;

import java.net.*;
import java.io.*;
import java.util.*;

public class ChatHandler extends Thread {
	/**
	 * Vector holding the clients handlers.
	 */
	protected static Vector<ChatHandler> handlers = new Vector<ChatHandler>();
	/**
	 * Socket of the connection.
	 */
	protected Socket socket;
	/**
	 * Input stream for the connection.
	 */
	protected DataInputStream input;
	/**
	 * Output stream for the connection.
	 */
	protected DataOutputStream output;
	/**
	 * String holding the client's username.
	 */
	private String username;
	/**
	 * Boolean value that represents the execution state.
	 * */
	private boolean Running;

	/**
	 * Class constructor. Initializes every client's socker and username.
	 * 
	 * @param socket
	 *            Connection's socket.
	 * @throws IOException
	 */
	public ChatHandler(Socket socket) throws IOException {
		this.socket = socket;
		username = "anonymous";
		input = new DataInputStream(new BufferedInputStream(
				socket.getInputStream()));
		output = new DataOutputStream(new BufferedOutputStream(
				socket.getOutputStream()));
		this.Running = true;
	}

	/**
	 * For each client: broadcast its messages.
	 */
	public void run() {
		try {
			// first message is the user name
			handlers.addElement(this);
			username = input.readUTF().toString();
			System.out.println(username + " has joined");
			broadcast(username + " has joined.");
			while (Running) {
				String msg = input.readUTF();
				broadcast(username + " - " + msg);
			}
		} catch (IOException ex) {
		} finally {
			handlers.removeElement(this);
			broadcast(username + " has left.");
			try {
				socket.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Stop the run method safely.
	 */
	public void stopRun() {
		this.Running = false;
	}

	/**
	 * Broadcast a message to all the client handlers.
	 * 
	 * @param message
	 *            String holding the message.
	 */
	protected static void broadcast(String message) {
		synchronized (handlers) {
			Enumeration<ChatHandler> e = handlers.elements();
			while (e.hasMoreElements()) {
				ChatHandler handler = e.nextElement();
				try {
					synchronized (handler.output) {
						handler.output.writeUTF(message);
					}
					handler.output.flush();
				} catch (IOException ex) {
					handler.stopRun();
				}
			}
		}
	}
}
